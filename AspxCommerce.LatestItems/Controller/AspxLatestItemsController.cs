﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SanchiCommerce.Core;
using System.Data;

namespace SanchiCommerce.LatestItems
{
    public class AspxLatestItemsController
    {
        public AspxLatestItemsController()
        {
        }
        
        public List<LatestItemsInfo> GetLatestItemsByCount(AspxCommonInfo aspxCommonObj, int count)
        {
            try
            {
                AspxLatestItemsProvider objLatestItems = new AspxLatestItemsProvider();
                List<LatestItemsInfo> LatestItems = objLatestItems.GetLatestItemsByCount(aspxCommonObj, count);
                return LatestItems;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LatestItemsInfo> LatestItemsList(int offset,int limit,int count,int sortBy,AspxCommonInfo aspxCommonObj, string attributes)
        {
            try
            {
                return AspxLatestItemsProvider.GetLatestItemsList(offset,limit,count,sortBy,aspxCommonObj,attributes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LatestItemsInfo> SpecialItemsList(int offset,int limit, AspxCommonInfo aspxCommonObj, int sortBy,string attributes)
        {
            try
            {
                return AspxLatestItemsProvider.GetSpecialItemsList(offset,limit, aspxCommonObj, sortBy, attributes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LatestItemsInfo> FeaturedItemsList(int offset, int limit, AspxCommonInfo aspxCommonObj, int sortBy,string attributes)
        {
            try
            {
                return AspxLatestItemsProvider.GetFeaturedItemsList(offset, limit, aspxCommonObj, sortBy, attributes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetLatestItemsInfo(AspxCommonInfo aspxCommonObj)
        {
            try
            {
                AspxLatestItemsProvider objLatestItems = new AspxLatestItemsProvider();
                return objLatestItems.GetLatestItemsByCount(aspxCommonObj);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LatestItemSettingInfo GetLatestItemSetting(AspxCommonInfo aspxCommonObj)
        {
            try
            {
                AspxLatestItemsProvider objLatestItems = new AspxLatestItemsProvider();
                LatestItemSettingInfo objLatestSetting = new LatestItemSettingInfo();
                objLatestSetting = objLatestItems.GetLatestItemSetting(aspxCommonObj);
                return objLatestSetting;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void LatestItemSettingUpdate(string SettingValues, string SettingKeys, AspxCommonInfo aspxCommonObj)
        {
            try
            {
                AspxLatestItemsProvider objLatestItems = new AspxLatestItemsProvider();
                objLatestItems.LatestItemSettingUpdate(SettingValues, SettingKeys, aspxCommonObj);
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public List<LatestItemRssInfo> GetLatestRssFeedContent(AspxCommonInfo aspxCommonObj, int count)
        {
            try
            {

                AspxLatestItemsProvider objLatestItem = new AspxLatestItemsProvider();
                List<LatestItemRssInfo> itemRss = objLatestItem.GetLatestItemRssContent(aspxCommonObj, count);
                return itemRss;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataSet LatestItemWithOptionInfo(AspxCommonInfo aspxCommonObjs, int sort)
        {
            try
            {
                return AspxLatestItemsProvider.LatestItemWithOptionInfo(aspxCommonObjs, sort);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataSet SpecialItemWithOptionInfo(AspxCommonInfo aspxCommonObjs, int sort)
        {
            try
            {
                return AspxLatestItemsProvider.SpecialItemWithOptionInfo(aspxCommonObjs, sort);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataSet FaturedItemWithOptionInfo(AspxCommonInfo aspxCommonObjs, int sort)
        {
            try
            {
                return AspxLatestItemsProvider.FaturedItemWithOptionInfo(aspxCommonObjs, sort);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public LatestItemSettingInfo GetLatestItemsOptionSetting(AspxCommonInfo aspxCommonObj)
        {
            try
            {
                AspxLatestItemsProvider objLatestItems = new AspxLatestItemsProvider();
                LatestItemSettingInfo objLatestSetting = new LatestItemSettingInfo();
                objLatestSetting = objLatestItems.GetLatestItemsOptionSetting(aspxCommonObj);
                return objLatestSetting;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static List<VariantCombination> CheckCostVariantCombinationbyItemID(int itemID, AspxCommonInfo aspxCommonObj, string costVarinatValueIDs)
        {
            try
            {
                return AspxLatestItemsProvider.CheckCostVariantCombinationbyItemID(itemID, aspxCommonObj, costVarinatValueIDs);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
