﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SanchiCommerce.Core
{
    [DataContract]
    [Serializable]
    class UserReview
    {
        private string _username;
        private string _review;
        private System.Nullable<decimal> _rating;
        private System.Nullable<int> _statusID;

        private System.Nullable<int> _userReviewID;
        public UserReview()
        {
        }

        public string Username
        {
            get
            {
                return this._username;
            }
            set
            {
                if ((this._username != value))
                {
                    this._username = value;
                }
            }
        }

        public System.Nullable<decimal> Rating
        {
            get
            {
                return this._rating;
            }
            set
            {
                if ((this._rating != value))
                {
                    this._rating = value;
                }
            }
        }
        public System.Nullable<int> StatusID
        {
            get
            {
                return this._statusID;
            }
            set
            {
                if ((this._statusID != value))
                {
                    this._statusID = value;
                }
            }
        }
        public string Review
        {
            get
            {
                return this._review;
            }
            set
            {
                if ((this._review != value))
                {
                    this._review = value;
                }
            }
        }


        public System.Nullable<int> UserReviewID
        {
            get
            {
                return this._userReviewID;
            }
            set
            {
                if ((this._userReviewID != value))
                {
                    this._userReviewID = value;
                }
            }
        }

    }
    public class UserReviewBasicInfo
    {
        public System.Nullable<int> UserReviewID { get; set; }


        public string UserName { get; set; }

        public string Review { get; set; }

        public System.Nullable<int> StatusID { get; set; }
        public System.Nullable<int> Rating { get; set; }
    }
}
