﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserReview.ascx.cs" Inherits="Modules_ReviewManagement_UserReview" %>
<script type="text/javascript">

    //<![CDATA[
    $(function () {
        $(".sfLocale").localize({
            moduleKey: AspxReviewManagement
        });
    });
      var lblReviewsFromHeading='<%=lblReviewsFromHeading.ClientID %>';
    //]]>
</script>


<div id="divShowUserRatingDetails">
    <div class="cssClassCommonBox Curve">
        <div class="cssClassHeader">
            <h1>
                <asp:Label ID="lblReviewsGridHeading" runat="server"
                    Text="Comments and Reviews" meta:resourcekey="lblReviewsGridHeadingResource1"></asp:Label>
            </h1>
            <div class="cssClassRssDiv">
                <a href="#" class="cssRssImage" style="display: none">
                    <img id="itemReviewRssImage" alt="" src="" title="" />
                </a>
            </div>
            <div class="cssClassHeaderRight">
                <div class="sfButtonwrapper">
                    <%--<p>
                        <button type="button" class="sfBtn" id="btnAddNewReview">
                            <span class="sfLocale icon-addnew">Add New Review/Rating</span>
                        </button>
                    </p>--%>
                    <%--<p>
                        <button type="button" class="sfBtn" id="btnDeleteSelected">
                            <span class="sfLocale icon-delete">Delete All Selected</span>
                        </button>
                    </p>--%>

                    <div class="cssClassClear">
                    </div>
                </div>
            </div>
            <div class="cssClassClear">
            </div>
        </div>
        <div class="sfGridwrapper">
            <div class="sfGridWrapperContent">
                <div class="sfFormwrapper sfTableOption">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <%--<td>
                                <label class="cssClassLabel sfLocale">
                                    Nick Name:</label>
                                <input type="text" id="txtSearchUserName" class="sfTextBoxSmall" />
                            </td>--%>
                           <%-- <td>
                                <label class="cssClassLabel sfLocale">
                                    Status:</label>
                                <select id="ddlStatus" class="sfListmenu">
                                    <option value="" class="sfLocale">--All--</option>
                                </select>
                            </td>--%>
                            
                            <td></td>
                        </tr>
                    </table>
                </div>
                <table id="MyReview" width="100%" border="0" cellpadding="0" style="display:block"></table>
            </div>
        </div>
    </div>
</div>
<div id="divUserRatingForm" style="display: none">
    <div class="cssClassCommonBox Curve">
        <div class="cssClassHeader">
            <h2>
                <asp:Label ID="lblReviewsFromHeading" runat="server"></asp:Label>
            </h2>
        </div>
        <div class="sfFormwrapper">
            <table border="0" id="tblEditReviewForm" class="cssClassPadding" width="100%">

             <tr>
                    <td class="cssClassTableLeftCol">
                        <label class="cssClassLabel sfLocale">
                            Nick Name:</label>
                    </td>
                    <td>
                       
                         <input type="text" id="NickName" name="name" class="sfInputbox required"
                            /><span class="cssClassRequired">*</span>
                    </td>
                </tr>
                      <tr id="trRating">
                    <td class="cssClassTableLeftCol">
                        <label class="cssClassLabel sfLocale">
                             Rating:</label>
                    </td>
                    <td>
                        <div id="divRating">
                        </div>
                        <span class="cssClassRatingTitle"></span>
                    </td>
                </tr>
                <tr id="trAddedOn">
                    <td class="cssClassTableLeftCol">
                        <label class="cssClassLabel sfLocale">
                            Added On:</label>
                    </td>
                    <td>
                        <label id="lblAddedOn">
                        </label>
                    </td>
                </tr>
             
                <tr>
                    <td class="cssClassTableLeftCol">
                        <label class="cssClassLabel sfLocale">
                            Review:</label>
                    </td>
                    <td>
                        <textarea id="txtUserReview" cols="50" rows="10" name="review" class="cssClassTextArea required">
                        </textarea><span class="cssClassRequired">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="cssClassTableLeftCol">
                        <label class="cssClassLabel sfLocale">
                            Status:</label>
                    </td>
                    <td>
                        <select id="selectUserStatus" class="sfListmenu">
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div class="sfButtonwrapper">
            <p>
                <button type="button" id="btnUserReviewBack" class="sfBtn">
                    <span class="sfLocale icon-arrow-slim-w">Back</span></button>
            </p>
           
            <p>
                <button type="button" id="btnSubmitUserReview" class="sfBtn">
                    <span class="sfLocale icon-save">Save</span></button>
            </p>
            <p>
                <button type="button" id="btnDeleteReview" class="sfBtn">
                    <span class="sfLocale icon-delete">Delete</span></button>
            </p>
        </div>
    </div>
</div>

<input type="hidden" id="hdnUserReview" />
