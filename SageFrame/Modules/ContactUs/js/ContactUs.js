﻿(function($) {
    $.feedbackTab = function(p) {
        p = $.extend
        ({
            PortalID: 1,
            ContactUsPath: '',
            UserName: '',
            subject: '',
            emailSucessMsg: '',
            UserModuleID:''
        }, p);
        var feedbackTab = {
            config: {
                isPostBack: false,
                async: false,
                cache: false,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: { data: '' },
                dataType: 'json',
                baseURL: p.ContactUsPath + 'Services/ContactUsWebService.asmx/',
                method: "",
                ModulePath: '',
                PortalID: p.PortalID,
                UserName: p.UserName,
                subject: p.subject,
                emailSucessMsg: p.emailSucessMsg
            },

            ContactUsSaveAndSendEmail: function (name, email, phone, subject, message) {
                //debugger;
                var param = JSON2.stringify({
                    name: name,
                    email: email,
                    phone: phone,
                    subject: subject,
                    message: message,
                    isActive: true,
                    portalID: feedbackTab.config.PortalID,
                    addedBy: feedbackTab.config.UserName,
                    UserModuleID: p.UserModuleID,
                    SecureToken: SageFrameSecureToken
                });
                $.ajax({
                    type: "POST",
                    url: feedbackTab.config.baseURL + "ContactUsAddAndSendEmail",
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //debugger;
                        feedbackTab.ClearForm();
                        $('a.feedback-tab').click();
                        jAlert(feedbackTab.config.emailSucessMsg);
                    },
                    error: function (data) {
                        //debugger;
                        alert('error');
                    }
                });
            },
            ClearForm: function() {
                $("#txtName").val('');
                $("#txtContactEmail").val('');
                $("#txtContactNo").val('');
                $("#txtMessage").val('');
                $('.Required').remove();
                $('.invalid').remove();
            },
            init: function() {
                $("#btnSubmit").off().on("click", function (event) {
                    //debugger;
                    var v = $('#form1').validate({
                        rules: {
                            name: { required: true },
                            email: { required: true },
                            phone: { required: true },
                            message: { required: true }
                        },
                        messages: {
                            name: { required: '*' },
                            email: { required: '*' },
                            phone: { required: '*' },
                            message: { required: '*' }
                        }
                    });
                    //if (v.form()) {
                    //    var name = $("#txtName").val();
                    //    var email = $("#txtContactEmail").val();
                    //    var phone = $("#txtContactNo").val();
                    //    var message = $("#txtMessage").val();
                    //    feedbackTab.ContactUsSaveAndSendEmail(name, email, phone, feedbackTab.config.subject, message);
                    //}
                    //return true;

                    if (v.form()) {
                        var name = $("#txtName").val();
                        var email = $("#txtContactEmail").val();
                        var phone = $("#txtContactNo").val();
                        var message = $("#txtMessage").val();
                        var mail_check_invalid1 = /^[A-Z0-9._]+@[0-9.-]+\.[A-Z]{2,6}$/i;
                        var mail_check_invalid2 = /^[0-9._]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;
                        var mail_check = /^[0-9._]+@[0-9.-]+\.[A-Z]{2,6}$/i;
                        var email_check = /^[A-Z0-9._]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;
                        if (!email_check.test(email)) {
                            $('#lblmessage').text(GetSystemLocale("Invalid Email")).css({ "visibility": "visible", "color": "red", "display": "inline" });
                            return false;
                        }
                        else if (mail_check.test(email)) {
                            $('#lblmessage').text(GetSystemLocale("Invalid Email")).css({ "visibility": "visible", "color": "red", "display": "inline" });
                            return false;
                        }
                        else if (mail_check_invalid1.test(email)) {
                            $('#lblmessage').text(GetSystemLocale("Invalid Email")).css({ "visibility": "visible", "color": "red", "display": "inline" });
                            return false;
                        }
                        else if (mail_check_invalid2.test(email)) {
                            $('#lblmessage').text(GetSystemLocale("Invalid Email")).css({ "visibility": "visible", "color": "red", "display": "inline" });
                            return false;
                        }
                       
                        else {
                            feedbackTab.ContactUsSaveAndSendEmail(name, email, phone, feedbackTab.config.subject, message);
                            $('#lblmessage').css({ "display": "none" })
                            
                        }
                       
                    } return true;

                });
                $("#btnReset").on('click', function() {
                    feedbackTab.ClearForm();
                });
            }
        };
        feedbackTab.init();
    };
    $.fn.feedback = function(p) {
        $.feedbackTab(p);
    };
})(jQuery);