﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemFilters.ascx.cs" Inherits="Modules_SanchiCommerce_DetailsBrowse_SeperateFilters" %>
<div id="divShopFilter" class="cssClassFilter" style="display: none;">
    <div id="tblFilter">
        <h2 class="cssClassLeftHeader">
            <p></p>
        </h2>
        <asp:Literal ID="ltrFilter" runat="server" EnableViewState="false"></asp:Literal>
    </div>
</div>
<script type="text/javascript">
    //<![CDATA[
    
    
    $(function () {
        $(".sfLocale").localize({
            moduleKey: DetailsBrowse
        });
    });
    var maxPrice = parseFloat('<%=maxPrice %>');
    var minPrice = parseFloat('<%=minPrice %>');
    var isCategoryHasItems = parseInt('<%=IsCategoryHasItems %>');
    var categorykey = "<%=Categorykey%>";
    var allowAddToCart = '<%=AllowAddToCart %>';
    var allowOutStockPurchase = '<%=AllowOutStockPurchase %>';
    var noImageCategoryDetailPath = '<%=NoImageCategoryDetailPath %>';
    var noOfItemsInRow = '<%=NoOfItemsInARow %>';
    var displaymode = '<%=ItemDisplayMode %>'.toLowerCase();
    var variantQuery = '<%=variantQuery%>';
   
</script>