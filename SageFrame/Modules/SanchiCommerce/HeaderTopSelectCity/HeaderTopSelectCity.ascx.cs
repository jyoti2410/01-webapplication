﻿using SageFrame.Web;
using SanchiCommerce.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Modules_SanchiCommerce_HeaderTopSelectCity_HeaderTopSelectCity : BaseAdministrationUserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        IncludeJs("HeaderTopSelectCity", "/Modules/Sage_Banner/js/SageBannerView.js", "/Modules/SanchiCommerce/HeaderTopSelectCity/js/HeaderTopSelectCity.js");        
    }
}