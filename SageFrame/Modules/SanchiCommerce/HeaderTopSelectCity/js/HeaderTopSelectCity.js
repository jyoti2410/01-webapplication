﻿var HeaderTopSelectCity = "";
var date = new Date();
date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
$(function () {
    var aspxCommonInfo = {
        StoreID: SageFramePortalID,
        PortalID: SageFramePortalID,
        UserName: SageFrameUserName,
        CultureName: SageFrameCurrentCulture,
        CustomerID: customerID,
        SessionCode: sessionCode
    };

    HeaderTopSelectCity = {
        init: function() {
        $.ajax({
            type: "POST",
            url: aspxservicePath + "AspxCoreHandler.ashx/GetAttributeValues",
            data: JSON2.stringify({ aspxCommonObj: aspxCommonInfo, attributeName: 'City' }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                $.cookie("defaultcityname", data.d[0].AttributeID + "@" + data.d[0].InputTypeID + "@" + data.d[0].Value, { expires: date, path: '/' });

                var options = '';
                $.each(data.d, function (index, v) {
                    options += "<option value=" + v.AttributeID + "@" + v.InputTypeID + "@" + v.Value + ">" + v.Value + "</option>";
                });

                $('#ddlHeaderCity').append(options);
                $('#listname').append(options);

                if ($.cookie("cityname") == null || $.cookie("cityname") == undefined) {

                    $(".dropdown-menu1").show();
                    $('html, body').css('overflowY', 'hidden');
                    $('body').append('<div id="fade"></div>');
                    $('#fade').css({ 'filter': 'alpha(opacity=80)' }).show();


                }
                else {
                    $('#ddlHeaderCity').val($.cookie("cityname"));
                    $('#CityLoaction').text($('#ddlHeaderCity').val().split('@')[2]);
                }
            }
        });
    }


    };
  
    setTimeout(function () {  HeaderTopSelectCity.init(); })

});

function DefaultCity() {
    if ($("#ddlHeaderCity option").each(function () {
        if ($(this).is(':selected')) {
    var properties = {
        textBoxBtnOk: 'Continue',
        textBoxBtnCancel: 'Cancel',
        onComplete: function (e) {
            if (e) {
        $.cookie("cityname", $('#ddlHeaderCity').val(), { expires: date, path: '/' });
        ShopingBag.ClearCartItems();
    }
    else { var attribute = $.cookie("cityname") != null ? $.cookie("cityname").split('@')[2] : ''; $('#ddlHeaderCity').val(attribute); }
    }
    }

    if ($('#cartItemCount span').text() > 0) { csscody.confirm("<h2>Alert</h2><p>City change at this point will clear yor cart.</p>", properties); }
    else {
        $.cookie("cityname", $('#ddlHeaderCity').val(), { expires: date, path: '/' });
        if ($('#CityLoaction').text() == $('#ddlHeaderCity').val().split('@')[2]) {
        $('.dropdown-menu1,#fade').fadeOut();
    }
    else
        window.location = '/Default.aspx';
    }
    }
    }));
    else { $.cookie("cityname", $('#ddlHeaderCity').val(), { expires: date, path: '/' }); }
}
function ShowPopup() {
    $(".dropdown-menu1").show();
    $('body').append('<div id="fade"></div>');
    $('#fade').css({ 'filter': 'alpha(opacity=80)' }).show();
    $('#fade').on("click", function () {
        $('.dropdown-menu1,#fade').fadeOut();
    });
}

