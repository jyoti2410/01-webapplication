﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SimpleSearch.ascx.cs"
    Inherits="Modules_AspxGeneralSearch_SimpleSearch" %>
<script type="text/javascript">
    function validateSearch(e) {
        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32 || keyCode == 0) ||(keyCode == 8 || keyCode == 46));
        if (!ret) {
            return ret;
        }            
    }
    </script>
<div class="cssClassSageSearchWrapper">
    <%--<div class="cssSearchButton" id="divGeneralSearch">
        <label class="i-search cssClassSearch sfLocale" id="lblGeneralSearch"></label>
    </div>--%>
    <%--<div id="StaticNav">
        <ul>
            <li><a href="javascript:void(0);">Promotion</a></li>
            <li><a href="javascript:void(0);">Payment</a></li>
            <li><a href="javascript:void(0);">Shipping</a></li>
            <li><a href="javascript:void(0);">Return</a></li>
            <li>Call Us: 8252 101 101</li>
        </ul>
    </div>--%>
    <div class="cssSearchContainer" <%--style="display: none"--%>>
        <ul>
            <li>
                <div id="sfFrontCategory" style="display: none">
                    <%--<div class="sfCategoryFrontMenuDropdown">
                        <asp:Literal ID="litSSCat" runat="server" EnableViewState="False" meta:resourcekey="litSSCatResource1"></asp:Literal>
                    </div>--%>
                    <input type="hidden" value="0" id="txtSelectedCategory" name="selectedCategory" />
                </div>
            </li>
            <li>
                <input type="text" id="txtSimpleSearchText" class="cssClassSageSearchBox" maxlength="75" onkeypress="return validateSearch(event)"/>
                <input type="button" id="btnSimpleSearch" class="cssClassSageSearchButton sfLocale" value="" />
                 <%--<span class="icon-search sfSearchPages cssClassSageSearchButton sfLocale" id="btnSimpleSearch" ></span>--%>
             
            
        </ul>
        <%--<asp:Literal ID="litTopSearch" runat="server" EnableViewState="False" meta:resourcekey="litTopSearchResource1"></asp:Literal>--%>
       
    </div>
</div>
<script type="text/javascript">
    //<![CDATA[

    $(function () {
        $.fn.SimpleSearchInit({
            ShowCategoryForSearch: '<%=ShowCategoryForSearch %>',
            EnableAdvanceSearch: '<%=EnableAdvanceSearch %>',
            ShowSearchKeyWords: '<%=ShowSearchKeyWords %>',
            ResultPage: '<%=ResultPage%>',
            AdvanceSearchPageName: '<%=AdvanceSearchPageName%>'
        });
      
        if ('<%=ShowCategoryForSearch %>'.toLowerCase() == 'true') {
            $("#sfFrontCategory").show();

        }
        if ('<%=EnableAdvanceSearch %>'.toLowerCase() == 'true') {
            $("#lnkAdvanceSearch").html(getLocale(AspxGeneralSearch, "Advanced Search"));
            $("#lnkAdvanceSearch").show();
        }
        if ('<%=ShowSearchKeyWords %>'.toLowerCase() == 'true') {
            $("#topSearch").show();
        }

     });
    //]]>
</script>