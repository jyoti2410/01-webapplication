﻿(function ($) {
    $(document).ready(function () {
        if ($('body').find('#BoxOverlay').length == 0) {
            csscody.initialize();
        }
    });
    var aspxCommonObj = function () {
        var aspxCommonInfo = {
            StoreID: SanchiCommerce.utils.GetStoreID(),
            PortalID: SanchiCommerce.utils.GetPortalID(),
            UserName: SanchiCommerce.utils.GetUserName(),
            CultureName: SanchiCommerce.utils.GetCultureName()
        };
        return aspxCommonInfo;
    };
    $.createUsersReview = function () {
        var imagesList = '';
        var UsersReview = {
            config: {
                isPostBack: false,
                async: false,
                cache: true,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: '{}',
                dataType: 'json',
                baseURL: SanchiCommerce.utils.GetAspxServicePath() + "AspxCommonHandler.ashx/",
                method: "",
                url: "",
                oncomplete: 0,
                ajaxCallMode: "",
                error: ""
            },
            ajaxCall: function (config) {
                $.ajax({
                    type: UsersReview.config.type,
                    contentType: UsersReview.config.contentType,
                    cache: UsersReview.config.cache,
                    async: UsersReview.config.async,
                    url: UsersReview.config.url,
                    data: UsersReview.config.data,
                    dataType: UsersReview.config.dataType,
                    success:UsersReview.ajaxsuccess,
                 
                    error: function (err) {
                        console.log(err);
                    },
                });
            },
            ajaxsuccess: function (data) {
               
                switch (UsersReview.config.ajaxCallMode) {
                    case 0:
                        if ($("#ctext").val() !== '' && document.getElementById('lblRate').innerText !== ''
                            //$("#txtUserNm").val() !== '' &&
                            ) {
                            toastr.info("Your review has been accepted for moderation.");
                           // csscody.info("<h2>" + getLocale(AspxUserReviewDetails, "Successful Message") + "</h2><p>" + getLocale(AspxUserReviewDetails, "Your review has been accepted for moderation.") + "</p>"); 
                            UsersReview.ClearReviewForm();
                        }
                        break;
            }
            },
            init: function () {
                var ReviewID = '';
                var aspxCommonObj = {
                    StoreID: SanchiCommerce.utils.GetStoreID(),
                    PortalID: SanchiCommerce.utils.GetPortalID(),
                    UserName: SanchiCommerce.utils.GetUserName(),
                    CultureName: SanchiCommerce.utils.GetCultureName()
                };
                $("#dynReviewDetailsForm").show();
                var param = { aspxCommonObj: aspxCommonObj };
                $('.SaveUpdateClick').on("click", function () {
                    var comment = $("#ctext").val();
                    var rating = $("#lblRate").text().split('-')[0];
                    var geopoint = null;
                    if ($("#ctext").val() == '') {
                        $('#ctext').css("border", "1px solid red");
                        $('#ctext').next('span').show();
                    }
                    else {
                        $('#ctext').css("border", "1px solid #000");
                        $('#ctext').next('span').hide();
                    }
                    if ($("#lblRate").text() == '') {
                        $('#lblRate').next('span').show();
                    }
                    else {
                        $('#lblRate').next('span').hide();
                    }
                   
                    if ($("#ctext").val() !== '' && $("#lblRate").text() !== ''  ) {
                        $('#checkinpopup').dialog('close'); $('body').css('overflowY', 'scroll');
                    }

                    UsersReview.SaveUserRatings();
                });
                UsersReview.BindPopUp();

            },
            SaveUserRatings: function () {
                var statusId = 2;
                var review = $("#ctext").val();
                var rating = $("#lblRate").text().split('-')[0];
                var nickName = SanchiCommerce.utils.GetUserName();

                var userIP = SanchiCommerce.utils.GetClientIP();
                var ratingSaveObject = {
                    UserName: nickName,
                    Review: review,
                    Rating: rating,
                    StatusID: statusId
                };
                this.config.url = this.config.baseURL + "SaveUserRating";
                this.config.data = JSON2.stringify({ ratingSaveObject: ratingSaveObject, aspxCommonObj: aspxCommonObj() });
                this.config.ajaxCallMode = 0;
                this.ajaxCall(this.config);
         
          

            },
            vars: {
                existReviewByUser: "",
                existReviewByIP: ""
            },
            BindPopUp: function () {
                var userName = SanchiCommerce.utils.GetUserName();
                UsersReview.ClearReviewForm();
                if (userName.toLowerCase() != "anonymouseuser") {
                }
            },
            ClearReviewForm: function () {
                $("#ctext").val('');
                $("#countleft").text('150');
                $('#ctext').next('span').hide();
                $('#lblRate').next('span').hide();
                $('#ctext').css("border", "1px solid #000");
                document.getElementById('Rating1').className = "Empty";
                document.getElementById('Rating2').className = "Empty";
                document.getElementById('Rating3').className = "Empty";
                document.getElementById('Rating4').className = "Empty";
                document.getElementById('Rating5').className = "Empty";
                document.getElementById('lblRate').innerText = "";
            },
            SaveItemRatings: function () { }
        }
        UsersReview.init();
    };
    $.fn.SageUsersReview = function (p) {
        $.createUsersReview(p);
    };

})(jQuery);
function Decide(option) {

    var score = "0";
    var temp = "";
    document.getElementById('lblRate').innerText = "";
    if (option == 1) {

        document.getElementById('Rating1').className = "Filled";
        document.getElementById('Rating2').className = "Empty";
        document.getElementById('Rating3').className = "Empty";
        document.getElementById('Rating4').className = "Empty";
        document.getElementById('Rating5').className = "Empty";
        temp = "1-Poor";
        score = option;
    }
    if (option == 2) {
        document.getElementById('Rating1').className = "Filled";
        document.getElementById('Rating2').className = "Filled";
        document.getElementById('Rating3').className = "Empty";
        document.getElementById('Rating4').className = "Empty";
        document.getElementById('Rating5').className = "Empty";
        temp = "2-Ok";
        score = option;

    }
    if (option == 3) {
        document.getElementById('Rating1').className = "Filled";
        document.getElementById('Rating2').className = "Filled";
        document.getElementById('Rating3').className = "Filled";
        document.getElementById('Rating4').className = "Empty";
        document.getElementById('Rating5').className = "Empty";
        temp = "3-Fair";
        score = option;
    }
    if (option == 4) {
        document.getElementById('Rating1').className = "Filled";
        document.getElementById('Rating2').className = "Filled";
        document.getElementById('Rating3').className = "Filled";
        document.getElementById('Rating4').className = "Filled";
        document.getElementById('Rating5').className = "Empty";
        temp = "4-Good";
        score = option;
    }
    if (option == 5) {
        document.getElementById('Rating1').className = "Filled";
        document.getElementById('Rating2').className = "Filled";
        document.getElementById('Rating3').className = "Filled";
        document.getElementById('Rating4').className = "Filled";
        document.getElementById('Rating5').className = "Filled";
        temp = "5-Nice";
        score = option;
    }
    document.getElementById('lblRate').innerText = temp;
}
function pop1() {
        $("#checkinpopup").dialog({ autoOpen: true, modal: true,responsive: true, closeOnEscape: false, resizable: false, beforeclose: function (event, ui) { return false; }, show: 'fade', hide: 'fade', open: function () { $('.ui-widget-overlay', this).hide().fadeIn(); $('.ui-icon-closethick').bind('click.close', function () { $('.ui-widget-overlay').fadeOut(function () { $('.ui-icon-closethick').unbind('click.close'); $('.ui-icon-closethick').trigger('click'); }); }); jQuery('button.ui-dialog-titlebar-close').hide(); }, }); $(".ui-dialog .ui-dialog-titlebar").css("display", "none"); $(".ui-dialog").css("width", "100%"); $(".ui-dialog").css("height", "100%"); $(".ui-dialog").css("padding", "0"); 
        $(".ui-dialog").css("top", "0%");
        $(".ui-dialog").css("position", "fixed");
        $(".ui-dialog").css("z-index", "9999");
        $(".ui-dialog").css("background", "none");
        $(".ui-dialog").css("left", "0%"); $(".ui-dialog").css("box-shadow", "none");
        $('body').css('overflowY', 'hidden');
        $(".ui-dialog").css('border-bottom-left-radius', '0px');
        $(".ui-resizable-n,.ui-resizable-s,.ui-resizable-w,.ui-resizable-e,.ui-resizable-se").css("cursor", "default"); $(".ui-widget-content .ui-icon").css("background-image", "none"); 
        $('.cssclose').click(function () {
            $('body').css('overflowY', 'scroll');
            $("#countleft").text("150");
            $('#ctext').next('span').hide();
            $('#lblRate').next('span').hide();
            $('#ctext').css("border", "1px solid #000");
            $("#ctext").val(''); document.getElementById('lblRate').innerText = ""; document.getElementById('Rating1').className = "Empty"; document.getElementById('Rating2').className = "Empty"; document.getElementById('Rating3').className = "Empty"; document.getElementById('Rating4').className = "Empty"; document.getElementById('Rating5').className = "Empty"; $('#checkinpopup').dialog('close'); $('body').css('overflowY', 'scroll');
        });

}
function pop() {
        $("#LoginPop").dialog({ autoOpen: true, modal: true, closeOnEscape: false, resizable: false, beforeclose: function (event, ui) { return false; }, show: 'fade', hide: 'fade', open: function () { $('.ui-widget-overlay', this).hide().fadeIn(); $('.ui-icon-closethick').bind('click.close', function () { $('.ui-widget-overlay').fadeOut(function () { $('.ui-icon-closethick').unbind('click.close'); $('.ui-icon-closethick').trigger('click'); }); }); jQuery('button.ui-dialog-titlebar-close').hide(); }, }); $(".ui-dialog .ui-dialog-titlebar").css("display", "none"); $(".ui-dialog").css("width", "100%"); $(".ui-dialog").css("height", "100%"); $(".ui-dialog").css("padding", "0"); $(".ui-dialog").css("border-radius", "inherit");
        $(".ui-dialog").css("top", "0%");
        $(".ui-dialog").css("left", "0%");
        $(".ui-dialog").css("position", "fixed");
        $(".ui-dialog").css("z-index", "9999");
        $(".ui-dialog").css("background", "none");
       $(".ui-dialog").css("box-shadow", "none");
        $('body').css('overflowY', 'hidden');
        $(".ui-dialog").css("border-radius", "inherit");
        $(".ui-resizable-n,.ui-resizable-s,.ui-resizable-w,.ui-resizable-e,.ui-resizable-se").css("cursor", "default"); $(".ui-widget-content .ui-icon").css("background-image", "none"); 
        $('.cssclose').click(function () {
            $('#LoginPop').dialog('close'); $('body').css('overflowY', 'scroll');
        });
       
}
